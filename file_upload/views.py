from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.contrib import messages
from .models import Document
from .forms import UploadFileForm


def upload_files(request):
    if request.method == 'POST':
        form = UploadFileForm(request.POST, request.FILES)
        if form.is_valid():
            newdoc = Document(docfile=request.FILES['docfile'])
            newdoc.save()
            #return HttpResponseRedirect(reversed('upload_files'))
            messages.success(request, 'File uploaded successfuly')
    else:
        form = UploadFileForm()

    documents = Document.objects.all()

    return render(request, 'home.html', {'documents': documents, 'form': form})


def handler404(request, exception):
    return render(request, '404.html', locals())

def handler400(request, exception):
    return render(request, '400.html', locals())

def handler500(request):
    return render(request, '500.html')
